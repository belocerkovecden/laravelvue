const critical = require('critical')

critical.generate({
    base: './resources/views',
    src: 'index.blade.php',
    css: ['./public/css/app.css'],
    width: 430,
    height: 600,
    target: {
        css: './public/css/critical.css',
        uncritical: './public/css/async.css',
    },
    //minify: true,
    //extract: true,
    // Включить класс подвала
    //include: ['.footer'],
    ignore: {
        atrule: ['@font-face'],
    }
});

