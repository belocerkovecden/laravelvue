<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

---

### Команды

- Создай зависимости  
  `npm i`  
  `composer install`  


- Оптимизация и Очистка кэша  
  `composer dumpautoload -o`  
  `php artisan config:cache & php artisan route:cache & php artisan cache:clear & php artisan config:clear & php artisan view:cache`  
  `php artisan migrate`  


- Для создания файла smart-grid.less используй команду  
  `node smartgrid.js`
  

- Для разработки  
  `php artisan serve`  
  `npm run watch`  
  

- Собрать и минифицировать js и css  
  `npm run prod`
  
  
- Создать критический и ассинхронный css файлы  
  `node critical.js`


- В случае 500 - ой ошибки проверь  
  `.htaccess`  
  `routes`  
  `.env`  
  `миграции`  


- Узнать занятый порт:  
`sudo lsof -t -i:80`  
`sudo kill 22940`


- Создание лайтовой ссылки для изображений  
`php artisan storage:link`  
  

- Генерация ключа для .env  
`php artisan key:generate`  


- .htaccess над пабликом
```
IfModule mod_rewrite.c>
    <IfModule mod_negotiation.c>
        Options -MultiViews
    </IfModule>
    
    RewriteEngine On

    RewriteCond %{HTTPS} !=on
    RewriteCond %{HTTP_HOST} ^(koenigsmann.ru)
    RewriteRule .* https://%{SERVER_NAME}%{REQUEST_URI}%

    RewriteCond %{REQUEST_FILENAME} -d [OR]
    RewriteCond %{REQUEST_FILENAME} -f
    RewriteRule ^ ^$1 [N]

    RewriteCond %{REQUEST_URI} (\.\w+$) [NC]
    RewriteRule ^(.*)$ public/$1

    RewriteCond %{REQUEST_FILENAME} !-d
    RewriteCond %{REQUEST_FILENAME} !-f
    RewriteRule ^ server.php

</IfModule>
```

- .htaccess в public
```
RewriteEngine On
RewriteBase /public/

RewriteCond %{HTTPS} !=on
RewriteCond %{HTTP_HOST} ^(koenigsmann.ru)
RewriteRule .* https://%{SERVER_NAME}%{REQUEST_URI}%


# Redirect Trailing Slashes If Not A Folder...
RewriteCond %{REQUEST_FILENAME} !-d
RewriteRule ^(.*)/$ /$1 [L,R=301]

# Authorization Headers
RewriteCond %{HTTP:Authorization} .
RewriteRule .* - [E=HTTP_AUTHORIZATION:%{HTTP:Authorization}]

# Handle Front Controller...
RewriteCond %{REQUEST_FILENAME} !-d
RewriteCond %{REQUEST_FILENAME} !-f
RewriteRule ^ index.php [L]
```

- Дать разрешения  
`sudo chown -R $USER:$USER /var/www/html/public`  
`chmod 755 project/`  
`chmod 755 project/public/`  
`chmod 644 project/public/index.php`  
`sudo apachectl -t -D DUMP_MODULES | grep rewrite`  
  

  >Крайний случай  
`chmod -R 777 storage`  
`chmod -R 777 bootstrap/cache`  
`chmod -R 0777 storage/`  


---

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
